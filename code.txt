<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<style>
    * {
        font-family: "Source Sans Pro", sans-serif;
        font-weight: 300;
        font-style: normal;
        color: #504f4d;
    }

    body {
        position: absolute;
        height: 100vh;
        width: 100vw;
    }

    .body-container {
        height: 100%;
    }

    .container-result {
        padding-bottom: 35px;
    }

    @media (max-width: 600px) {
        .container-result {
            padding-top: 25px;
        }
    }

    .row-col-white .col-3 {
        background-color: #ffffff;
    }

    .row-col-white .col-9 {
        background-color: #ffffff;
    }

    .col-border-top {
        border-top: 1px solid #2e2e2e;
    }

    .col-border-right {
        border-right: 1px solid #2e2e2e;
    }

    .col-border-bottom {
        border-bottom: 1px solid #2e2e2e;
    }

    .col-border-left {
        border-left: 1px solid #2e2e2e;
    }

    .col-radius-lt {
        border-radius: 30px 0px 0px 0px;
    }

    .col-radius-tr {
        border-radius: 0px 30px 0px 0px;
    }

    .col-radius-rb {
        border-radius: 0px 0px 30px 0px;
    }

    .col-radius-bl {
        border-radius: 0px 0px 0px 30px;
    }

    .container-result-title {
        font-size: 32px;
        letter-spacing: 5px;
    }

    @media (max-width: 600px) {
        .container-result-title {
            margin-right: -15px;
            margin-left: -15px;
            font-size: 18px;
            letter-spacing: 1px;
        }
    }

    .body-row {
        height: 100%;
    }

    .bg-turquoise {
        background-color: #e5cec0;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#d9baa6), to(#faf2ee));
        background-image: linear-gradient(to bottom, #d9baa6, #faf2ee);
    }

    .bg-image-1 {
        position: relative;
    }

    .bg-image-1::after {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-image: url(../img/bg.PNG);
        background-position: center center;
        background-size: cover;
        background-repeat: no-repeat;
        opacity: 0.21;
        z-index: -1;
    }

    .bg-image-1::before {
        content: "";
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background-color: #d6d8da;
        z-index: -1;
    }

    .h1 {
        font-size: 40px;
        letter-spacing: 10px;
    }

    .h2 {
        font-size: 32px;
        letter-spacing: 5px;
    }

    @media (max-width: 600px) {
        .h2 {
            font-size: 18px;
            letter-spacing: 1px;
        }
    }

    @media (max-width: 600px) {

        h1,
        h2,
        h3,
        h4,
        h5,
        .h1,
        .h2,
        .h3,
        .h4,
        .h5 {
            line-height: 1;
            letter-spacing: 7px;
        }

        .h1 {
            font-size: 24px;
        }
    }

    @media (max-width: 600px) {

        h1,
        h2,
        h3,
        h4,
        h5,
        .h1,
        .h2,
        .h3,
        .h4,
        .h5 {
            font-size: 18px;
            letter-spacing: 2px;
        }
    }

    .card-pythagoras {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        margin: 15px 0;
        padding: 15px 0;
        color: #000000;
        font-size: 18px;
        letter-spacing: 2px;
        -webkit-transition: all 300ms;
        transition: all 300ms;
    }

    .card-pythagoras:hover {
        -webkit-transform: scale(1.1);
        transform: scale(1.1);
    }

    .card-pythagoras-info {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: vertical;
        -webkit-box-direction: normal;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
        margin: 0;
        padding: 0;
        color: #000000;
        font-size: 18px;
        letter-spacing: 2px;
    }

    .card-pythagoras-info .card-pythagoras-row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: flex-start;
    }

    .card-pythagoras-row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-orient: horizontal;
        -webkit-box-direction: normal;
        -ms-flex-direction: row;
        flex-direction: row;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
    }

    .form-calc {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        margin-bottom: 2em;
    }

    @media (max-width: 600px) {
        .form-calc {
            padding: 0;
        }
    }

    .form-title {
        margin: 48px 0;
    }

    @media (max-width: 600px) {
        .form-title {
            margin: 24px 0 0 0;
        }
    }

    .form-group {
        margin: 48px 0;
    }

    @media (max-width: 600px) {
        .form-group {
            margin: 12px 0;
        }
    }

    .form-calc-group {
        position: relative;
    }

    @media (max-width: 600px) {
        .form-calc-group {
            width: 100%;
        }
    }

    .form-calc-control {
        border: #ffffff;
        outline: none;
        font-size: 22px;
        text-align: center;
        border: 1px solid #ffffff;
        border-radius: 50px;
        -webkit-transition: all 300ms;
        transition: all 300ms;
        outline: none;
        -webkit-box-shadow: none;
        box-shadow: none;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        width: 100%;
        color: #31322f;
    }

    .form-calc-control.is-invalid {
        border-color: red;
        color: red;
    }

    @media (max-width: 600px) {
        .form-calc-control {
            width: 100%;
            padding: 0.5rem;
        }
    }

    .form-calc-submit {
        padding: 1em;
        border: #e6f0db;
        font-size: 18px;
        background-color: #e6f0db;
        outline: none;
        color: #31322f;
        -webkit-transition: all 300ms;
        transition: all 300ms;
        border: 1px solid #31322f;
    }

    .form-calc-submit:hover {
        background-color: #c7d1bc;
    }

    @media (max-width: 600px) {
        .form-calc-submit {
            padding: 0.5em 0;
            width: 100%;
        }
    }

    .form-calc-label-hover {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        font-size: 18px;
        margin: 0;
        color: #31322f;
    }

    @media (max-width: 600px) {
        body {
            position: relative;
            height: auto;
            width: auto;
        }

        .mobile-grid {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
        }

        .card-pythagoras {
            margin: 1px 0;
            font-size: 12px;
            letter-spacing: 0px;
            margin-left: -14px;
            margin-right: -14px;
        }

        .mobile-a {
            padding: 10px 0px 5px 0 !important;
        }
    }

    img {
        display: none !important;
    }

    .data {
        margin: 0 10px;
        width: 600px;
    }

    .gb-color-1 {
        background-color: #e6f0db !important;
    }

    /*# sourceMappingURL=style.min.css.map */
</style>
<div class="body-container container m-auto">
    <div class="body-row row">
        <div class="col-12">
            <div class="container-result">
                <div class="row">
                    <div class="col-12">
                        <form id="FormCalc" class="row form-calc mt-5">
                            <div class="col-md-3 col-12 d-flex justify-content-center align-items-center">
                                <h3 class="h3 text-center">Введите дату рождения:</h3>
                            </div>
                            <div class="col-md-6 col-12 mt-md-0 mt-3 d-flex justify-content-center align-items-center" style="border-bottom: 1px solid #504f4d;">
                                <div class="form-calc-group">
                                    <input type="text" class="form-calc-control" name="date" id="InputDate" value="">
                                    <label for="InputDate" class="form-calc-label-hover">ДД.ММ.ГГГГ</label>
                                </div>
                            </div>
                            <div class="col-md-3 col-12 d-flex mt-md-0 mt-3">
                                <button type="submit" class="form-calc-submit flex-grow-1">Рассчитать</button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center align-items-center">
                        <div class="data">
                            <div class="row row-col-white">
                                <div class="col-9 d-flex align-items-end col-border-top col-border-right col-border-bottom col-border-left gb-color-1">
                                    <div class="card-pythagoras-info">
                                        <div class="card-pythagoras-row">
                                            <h5><span class="h5">Дата рождения:</span> <span id="ValueDate">--.--.----</span></h5>
                                        </div>
                                        <div class="card-pythagoras-row">
                                            <h5><span class="h5">Доп. числа:</span> <span id="NumberAdditionalNumber">--.-- --.--</span></h5>
                                        </div>
                                        <div class="card-pythagoras-row">
                                            <h5><span class="h5">Число судьбы:</span> <span id="NumberFate"></span></h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3 col-border-top col-border-right col-border-bottom gb-color-1">
                                    <div class="card-pythagoras">
                                        <span>Темперамент</span>
                                        <span id="temperament">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom col-border-left">
                                    <div class="card-pythagoras">
                                        <span>Характер</span>
                                        <span id="character">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Здоровье</span>
                                        <span id="health">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Удача</span>
                                        <span id="luck">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom gb-color-1">
                                    <div class="card-pythagoras">
                                        <span>Цель</span>
                                        <span id="target">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom col-border-left">
                                    <div class="card-pythagoras">
                                        <span>Энергия</span>
                                        <span id="energy">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Логика</span>
                                        <span id="logics">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Долг</span>
                                        <span id="debt">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom gb-color-1">
                                    <div class="card-pythagoras">
                                        <span>Семья</span>
                                        <span id="family">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom col-border-left">
                                    <div class="card-pythagoras">
                                        <span>Интерес</span>
                                        <span id="interest">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Труд</span>
                                        <span id="work">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom">
                                    <div class="card-pythagoras">
                                        <span>Память</span>
                                        <span id="memory">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom gb-color-1">
                                    <div class="card-pythagoras">
                                        <span>Привычки</span>
                                        <span id="habits">---</span>
                                    </div>
                                </div>
                                <div class="col-3 col-border-right col-border-bottom col-border-left">
                                    <div class="card-pythagoras">
                                        <span>Быт</span>
                                        <span id="everyday">---</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center align-items-center">
                        <h2 class="h1 m-3">topolskova_numerolog</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $("#FormCalc input[name=date]").inputmask("99.99.9999");
        $("#FormCalc input[name=date]").keyup(function (e) {
            var id = $(this).attr("id");
            var value = $(this).val();
            if (value.length > 0) $("label[for=" + id + "]").hide();
            else $("label[for=" + id + "]").show();
        });
        $("#FormCalc").submit(function (e) {
            e.preventDefault();
            var value = $("#FormCalc input[name=date]").val();
            if (value.length == 0) {
                $("#FormCalc input[name=date]").addClass("is-invalid");
                return;
            } else $("#FormCalc input[name=date]").removeClass("is-invalid");
            var arr;
            var dateOfBirth = $(this).find("input[name=date]").val();
            var strDate = dateOfBirth + "";
            var aDate = strDate.split('.');
            if (aDate.length != 3) alert("Не верный формат даты! Пример: 01.01.2020");
            var nDay = parseInt(aDate[0]);
            var nMonth = parseInt(aDate[1]);
            var nYear = parseInt(aDate[2]);
            numberOne = 0;
            if (nDay < 10) numberOne += nDay;
            else {
                arr = (nDay + "").split('');
                for (let i = 0; i < arr.length; i++) numberOne += parseInt(arr[i]);
            }
            if (nMonth < 10) numberOne += nMonth;
            else {
                arr = (nMonth + "").split('');
                for (let i = 0; i < arr.length; i++) numberOne += parseInt(arr[i]);
            }
            arr = (nYear + "").split('');
            for (let i = 0; i < arr.length; i++) numberOne += parseInt(arr[i]);
            numberOne = parseInt(numberOne);
            var numberTwo = 0;
            if (numberOne < 10) numberTwo = numberOne;
            else {
                arr = (numberOne + "").split('');
                for (let i = 0; i < arr.length; i++) numberTwo += parseInt(arr[i]);
            }
            numberTwo = parseInt(numberTwo);
            var numberThree = 0;
            if (nDay < 10) numberThree = numberOne - (2 * nDay);

            else {
                arr = (nDay + "").split('');
                numberThree = numberOne - (2 * arr[0]);
            }
            numberThree = parseInt(numberThree);
            var numberFour = 0;
            if (numberThree < 10) numberFour = numberThree;
            else {
                arr = (numberThree + "").split('');
                for (let i = 0; i < arr.length; i++)  numberFour += parseInt(arr[i]);
            }
            numberFour = parseInt(numberFour);
            var numberFate = 0;
            arr = (numberOne + "").split('');
            for (let i = 0; i < arr.length; i++)  numberFate += parseInt(arr[i]);
            for (let i = 0; i < 99; i++) {
                if (numberFate < 10 || numberFate == 11) break;
                arr = (numberFate + "").split('');
                numberFate = 0;
                for (let j = 0; j < arr.length; j++)  numberFate += parseInt(arr[j]);
            }
            var strNumberAll = "" + nDay + nMonth + nYear + numberOne + numberTwo + numberThree + numberFour;
            arr = strNumberAll.split('');
            var character = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 1) character += arr[i];
            var energy = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 2) energy += arr[i];;
            var interest = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 3) interest += arr[i];;
            var health = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 4) health += arr[i];
            var logics = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 5) logics += arr[i];
            var work = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 6) work += arr[i];
            var luck = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 7) luck += arr[i];
            var debt = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 8) debt += arr[i];;
            var memory = "";
            for (let i = 0; i < arr.length; i++) if (parseInt(arr[i]) == 9) memory += arr[i];
            var temperament = interest.length + logics.length + luck.length;
            var everyday = health.length + logics.length + work.length;
            var target = character.length + health.length + luck.length;
            var family = energy.length + logics.length + debt.length;
            var habits = interest.length + work.length + memory.length;
            var day = nDay + "";
            if (nDay < 10) day = "0" + day;
            var month = nMonth + "";
            if (nMonth < 10) month = "0" + month;
            $("#ValueDate").html(day + "." + month + "." + nYear);
            $("#NumberAdditionalNumber").html(numberOne + "." + numberTwo + " " + numberThree + "." + numberFour);
            $("#NumberFate").html(numberFate);
            if (character == "") character = "---";
            $("#character").html(character);
            if (health == "") health = "---";
            $("#health").html(health);
            if (luck == "") luck = "---";
            $("#luck").html(luck);
            if (energy == "") energy = "---";
            $("#energy").html(energy);
            if (logics == "") logics = "---";
            $("#logics").html(logics);
            if (debt == "") debt = "---";
            $("#debt").html(debt);
            if (interest == "") interest = "---";
            $("#interest").html(interest);
            if (work == "") work = "---";
            $("#work").html(work);
            if (memory == "") memory = "---";
            $("#memory").html(memory);
            if (temperament == "") temperament = "---";
            $("#temperament").html(temperament);
            if (everyday == "") everyday = "---";
            $("#everyday").html(everyday);
            if (target == "") target = "---";
            $("#target").html(target);
            if (family == "") family = "---";
            $("#family").html(family);
            if (habits == "") habits = "---";
            $("#habits").html(habits);
        });
    });
</script>